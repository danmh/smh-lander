---
permalink: /404.html
layout: landing
title: Stiwdio Mizzi‑Harris
subtitle: Commercial Interior Design services for industry and property developers
footer: 404 — this page not found on mizziharris.com
---

See Becca's work at [portfolio.mizziharris.com](https://portfolio.mizziharris.com)