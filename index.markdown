---
layout: landing

title: Stiwdio Mizzi‑Harris
subtitle: Commercial Interior Design services for industry and property developers
footer: Full services website launching A/W 2022.
---

See Becca's work at [portfolio.mizziharris.com](https://portfolio.mizziharris.com)

